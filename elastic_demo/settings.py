"""
Django settings for elastic_demo project.

"""

import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

SECRET_KEY = 'cj+52a2q1r_o@=x!u*b21fe70igda7-d7r%7@7o-&$-s0x4%=o'

DEBUG = True

ALLOWED_HOSTS = []


INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'elastic_demo.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, "templates"), ],
        'APP_DIRS': False,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.core.context_processors.request',
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
                'admin_tools.template_loaders.Loader',
            ]
        },
    },
]

WSGI_APPLICATION = 'elastic_demo.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'elastic_demo',
        'USER': 'kovalenko',
        'PASSWORD': 'j0ker',
        'HOST': '127.0.0.1',
        'PORT': '',
    }
}


LANGUAGE_CODE = 'ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATICFILES_DIRS = ()


MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, "media")

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    # 'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)
THUMBNAIL_HIGH_RESOLUTION = True

# ########## Elastic
# Depends apps
INSTALLED_APPS += (
    'autocomplete_light',
    'mptt',
    'mpttadmin',
    'easy_thumbnails',
    'filer',
    'ckeditor',
    'django_ace',
    'taggit',
    'gunicorn',
    'sekizai',
)
# Base apps
INSTALLED_APPS += (
    'elastic.currency.apps.ElasticCurrency',
    'elastic.country.apps.ElasticCountry',
    'elastic.address.apps.ElasticAddress',
    'elastic.party.apps.ElasticParty',
    'elastic.product.apps.ElasticProduct',
    'elastic.workflow.apps.ElasticWorkflow',
    'elastic.stock.apps.ElasticStock',
    'elastic.delivery.apps.ElasticDelivery',
    'elastic.articles.apps.ElasticArticle',
    'elastic.organization.apps.ElasticOrganization',
    'elastic.product_catalog.apps.ElasticProductCatalog',
)

# ########## Admin-Tools
INSTALLED_APPS = (
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    # 'feedparser',
) + INSTALLED_APPS


# ########### ck-editor
# CKEDITOR_SETTINGS = {
#    'language': '{{ language }}',
#    # 'toolbar': 'CMS',
#    'toolbar': 'Full',
#    'skin': 'moono',
# }
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'CMS',
        'height': 300,
        'width': 700,
    },
}
CKEDITOR_MEDIA_PREFIX = os.path.join(MEDIA_ROOT, 'ckeditor')
CKEDITOR_UPLOAD_PATH = "uploads/"

ELASTIC_SETTINGS = {
    'server_name': "demo",
}

MQ_SERVER = {
    'host': '127.0.0.1',
    'port': '5672',
    'username': 'guest',
    'password': 'j0ker',
    'virtual_host': '/',
    'ssl': False,
}
