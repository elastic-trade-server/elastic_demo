from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^autocomplete/', include('autocomplete_light.urls')),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^elastic/country/', include('elastic.country.urls')),
    url(r'^elastic/currency/', include('elastic.currency.urls')),
    url(r'^elastic/address/', include('elastic.address.urls')),
    url(r'^elastic/party/', include('elastic.party.urls')),
    url(r'^elastic/product/', include('elastic.product.urls')),
    url(r'^elastic/stock/', include('elastic.stock.urls')),
    url(r'^elastic/organization/', include('elastic.organization.urls')),
    url(r'^elastic/delivery/', include('elastic.delivery.urls')),
    # url(r'^elastic/sb/', include('elastic.sb.urls')),
    # url(r'^elastic/sb_sync_adapter/', include('elastic.sb.sync_adapter.urls')),
]
